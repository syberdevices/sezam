#ifndef BLE_APS_H__
#define BLE_APS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nrf_sdh_ble.h"
#include "nrf_ble_gatt.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_APS_BLE_OBSERVER_PRIO              2


/**@brief   Macro for defining a ble_aps instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_APS_DEF(_name)                                                                          \
static ble_aps_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_APS_BLE_OBSERVER_PRIO,                                                     \
                     ble_aps_on_ble_evt, &_name)


// Forward declaration of the ble_aps_t type.
typedef struct ble_aps_s ble_aps_t;

/**@brief Control Service event handler type. */
typedef void (*ble_aps_write_evt_handler_t) (ble_aps_t * p_aps, ble_gatts_evt_write_t const * p_evt);

/**@brief Control Service init structure. This contains all options and data needed for
 *        initialization of the service. */
typedef struct
{
    ble_aps_write_evt_handler_t  write_evt_handler;                                    /**< Event handler to be called for handling events in the Control Service. */
    bool                         access;
} ble_aps_init_t;

/**@brief Control Service structure. This contains various status information for the service. */
struct ble_aps_s
{
    ble_aps_write_evt_handler_t  write_evt_handler;  
    uint16_t                     service_handle;                                       /**< Handle of Control Service (as provided by the BLE stack). */
    uint16_t                     conn_handle;                                          /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    ble_gatts_char_handles_t     buuid_handles;                                          /**< Handles related to the Command characteristic. */
    ble_gatts_char_handles_t     maj_handles;                                          /**< Handles related to the Response characteristic. */
    ble_gatts_char_handles_t     min_handles;                                          /**< Handles related to the Key characteristic. */
    ble_gatts_char_handles_t     tx_pwr_handles;                                         /**< Handles related to the Hash characteristic. */
    ble_gatts_char_handles_t     tx_period_handles;                                         /**< Handles related to the Hash characteristic. */
    uint8_t                      max_len;                                              /**< Current maximum length, adjusted according to the current ATT MTU. */
};


/**@brief Function for initializing the Control Service.
 *
 * @param[out]  p_aps       Control Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_aps_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_aps_init(ble_aps_t * p_aps, ble_aps_init_t const * p_aps_init);


/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the Control Service.
 *
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 * @param[in]   p_context   Control Service structure.
 */
void ble_aps_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);


#ifdef __cplusplus
}
#endif

#endif // BLE_APS_H__

