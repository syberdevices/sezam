
#include "sdk_common.h"
#include "ble_aps.h"
#include <string.h>
#include "ble_srv_common.h"
#include "common.h"


#define OPCODE_LENGTH 1                                                              /**< Length of opcode inside Control Measurement packet. */
#define HANDLE_LENGTH 2                                                              /**< Length of handle inside Control Measurement packet. */
#define MAX_APS_LEN      (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum size of a transmitted Control Measurement. */

#define APS_UUID_BASE                   CUSTOM_UUID_BASE
#define APS_UUID_SERVICE		0xE200

#define BUUID_UUID_CHAR		        0xE201
#define BUUID_UUID_CHAR_LEN             16

#define MAJ_UUID_CHAR                   0xE202
#define MAJ_UUID_CHAR_LEN               2

#define MIN_UUID_CHAR                   0xE203
#define MIN_UUID_CHAR_LEN               2

#define TX_PWR_UUID_CHAR                0xE204
#define TX_PWR_UUID_CHAR_LEN            1

#define TX_PERIOD_UUID_CHAR             0xE205
#define TX_PERIOD_UUID_CHAR_LEN         2


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_aps       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_aps_t * p_aps, ble_evt_t const * p_ble_evt)
{
    p_aps->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}


/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_aps       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_aps_t * p_aps, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_aps->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief Function for handling the Write event.
 *
 * @param[in]   p_aps       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_aps_t * p_aps, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if ((p_evt_write->len > 0) && (p_aps->write_evt_handler != NULL)) {
        p_aps->write_evt_handler(p_aps, p_evt_write);
    }
}


void ble_aps_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_aps_t * p_aps = (ble_aps_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_aps, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_aps, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_aps, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_aps_init(ble_aps_t * p_aps, const ble_aps_init_t * p_aps_init)
{
    ble_uuid_t ble_uuid;
    ble_uuid128_t base_uuid = {APS_UUID_BASE};
    ble_uuid.uuid = APS_UUID_SERVICE;
    p_aps->conn_handle = BLE_CONN_HANDLE_INVALID;

    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &ble_uuid.type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_add_char_params_t add_char_params;

    // Initialize service structure
    p_aps->write_evt_handler           = p_aps_init->write_evt_handler;
    p_aps->conn_handle                 = BLE_CONN_HANDLE_INVALID;
    p_aps->max_len                     = MAX_APS_LEN;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_aps->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add beacon UUID characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = BUUID_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = BUUID_UUID_CHAR_LEN;
    add_char_params.init_len          = BUUID_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_aps->service_handle, &add_char_params, &(p_aps->buuid_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add major characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = MAJ_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = MAJ_UUID_CHAR_LEN;
    add_char_params.init_len          = MAJ_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_aps->service_handle, &add_char_params, &(p_aps->maj_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add minor characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = MIN_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = MIN_UUID_CHAR_LEN;
    add_char_params.init_len          = MIN_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_aps->service_handle, &add_char_params, &(p_aps->min_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add tx power characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = TX_PWR_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = TX_PWR_UUID_CHAR_LEN;
    add_char_params.init_len          = TX_PWR_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_aps->service_handle, &add_char_params, &(p_aps->tx_pwr_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }
	
	// Add tx period characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = TX_PERIOD_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = TX_PERIOD_UUID_CHAR_LEN;
    add_char_params.init_len          = TX_PERIOD_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_aps->service_handle, &add_char_params, &(p_aps->tx_period_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}



