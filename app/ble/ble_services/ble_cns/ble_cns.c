
#include "sdk_common.h"
#include "ble_cns.h"
#include <string.h>
#include "ble_srv_common.h"
#include "common.h"


#define OPCODE_LENGTH 1                                                              /**< Length of opcode inside Control Measurement packet. */
#define HANDLE_LENGTH 2                                                              /**< Length of handle inside Control Measurement packet. */
#define MAX_CNS_LEN      (NRF_SDH_BLE_GATT_MAX_MTU_SIZE - OPCODE_LENGTH - HANDLE_LENGTH) /**< Maximum size of a transmitted Control Measurement. */

#define CNS_UUID_BASE                   CUSTOM_UUID_BASE
#define CNS_UUID_SERVICE		0xE100

#define CMD_UUID_CHAR		        0xE101
#define CMD_UUID_CHAR_LEN               20

#define RSP_UUID_CHAR                   0xE102
#define RSP_UUID_CHAR_LEN               20

#define KEY_UUID_CHAR                   0xE103
#define KEY_UUID_CHAR_LEN               4

#define HASH_UUID_CHAR                  0xE104
#define HASH_UUID_CHAR_LEN              4


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_cns       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_cns_t * p_cns, ble_evt_t const * p_ble_evt)
{
    p_cns->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Function for handling the Disconnect event.
 *
 * @param[in]   p_cns       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_cns_t * p_cns, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_cns->conn_handle = BLE_CONN_HANDLE_INVALID;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_cns       Control Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_cns_t * p_cns, ble_evt_t const * p_ble_evt)
{
    ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if ((p_evt_write->len > 0) && (p_cns->write_evt_handler != NULL)) {
        p_cns->write_evt_handler(p_cns, p_evt_write);       
    }
}


void ble_cns_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
    ble_cns_t * p_cns = (ble_cns_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_cns, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_cns, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_cns, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}


uint32_t ble_cns_init(ble_cns_t * p_cns, const ble_cns_init_t * p_cns_init)
{
    ble_uuid_t ble_uuid;
    ble_uuid128_t base_uuid = {CNS_UUID_BASE};
    ble_uuid.uuid = CNS_UUID_SERVICE;
    p_cns->conn_handle = BLE_CONN_HANDLE_INVALID;

    uint32_t err_code = sd_ble_uuid_vs_add(&base_uuid, &ble_uuid.type);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    ble_add_char_params_t add_char_params;

    // Initialize service structure
    p_cns->write_evt_handler           = p_cns_init->write_evt_handler;
    p_cns->conn_handle                 = BLE_CONN_HANDLE_INVALID;
    p_cns->max_len                     = MAX_CNS_LEN;

    // Add service
    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid, &p_cns->service_handle);
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Cmd characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = CMD_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = CMD_UUID_CHAR_LEN;
    add_char_params.init_len          = CMD_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.char_props.read   = 1;
    add_char_params.write_access      = SEC_OPEN;
    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_cns->service_handle, &add_char_params, &(p_cns->cmd_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Rsp characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = RSP_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = RSP_UUID_CHAR_LEN;
    add_char_params.init_len          = RSP_UUID_CHAR_LEN;
    add_char_params.char_props.notify = 1;
//    add_char_params.char_props.read   = 1;
    add_char_params.cccd_write_access = SEC_OPEN;
//    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_cns->service_handle, &add_char_params, &(p_cns->rsp_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Key characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = KEY_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = KEY_UUID_CHAR_LEN;
    add_char_params.init_len          = KEY_UUID_CHAR_LEN;
    add_char_params.char_props.notify = 1;
//    add_char_params.char_props.read   = 1;
    add_char_params.cccd_write_access = SEC_OPEN;
//    add_char_params.read_access       = SEC_OPEN;

    err_code = characteristic_add(p_cns->service_handle, &add_char_params, &(p_cns->key_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    // Add Hash characteristic
    memset(&add_char_params, 0, sizeof(add_char_params));

    add_char_params.uuid              = HASH_UUID_CHAR;
    add_char_params.uuid_type         = ble_uuid.type;
    add_char_params.max_len           = HASH_UUID_CHAR_LEN;
    add_char_params.init_len          = HASH_UUID_CHAR_LEN;
    add_char_params.char_props.write  = 1;
    add_char_params.write_access      = SEC_OPEN;

    err_code = characteristic_add(p_cns->service_handle, &add_char_params, &(p_cns->hash_handles));
    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    return NRF_SUCCESS;
}


uint32_t ble_cns_key_send(ble_cns_t * p_cns, uint32_t key)
{
    uint32_t err_code;

    // Send value if connected and notifying
    if (p_cns->conn_handle != BLE_CONN_HANDLE_INVALID)
    {                
        uint16_t hvx_len = sizeof(key);
        ble_gatts_hvx_params_t hvx_params;

        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_cns->key_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = (uint8_t *)&key;

        err_code = sd_ble_gatts_hvx(p_cns->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;
}

uint32_t ble_cns_rsp_send(ble_cns_t * p_cns, uint8_t rsp)
{
    uint32_t err_code;

    // Send value if connected and notifying
    if (p_cns->conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        uint16_t hvx_len = sizeof(rsp);
        ble_gatts_hvx_params_t hvx_params;
 
        memset(&hvx_params, 0, sizeof(hvx_params));

        hvx_params.handle = p_cns->rsp_handles.value_handle;
        hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
        hvx_params.offset = 0;
        hvx_params.p_len  = &hvx_len;
        hvx_params.p_data = &rsp;

        err_code = sd_ble_gatts_hvx(p_cns->conn_handle, &hvx_params);
        if ((err_code == NRF_SUCCESS))
        {
            err_code = NRF_ERROR_DATA_SIZE;
        }
    }
    else
    {
        err_code = NRF_ERROR_INVALID_STATE;
    }

    return err_code;
}


