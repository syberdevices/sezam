/**
 * Copyright (c) 2015 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/**
 * @brief Blinky Sample Application main file.
 *
 * This file contains the source code for a sample server application using the LED Button service.
 */

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_err.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_state.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "app_timer.h"
#include "peer_manager.h"
#include "peer_manager_handler.h"
#include "ble_wgd.h"
#include "nrf_ble_gatt.h"
#include "nrf_ble_qwr.h"
#include "nrf_drv_gpiote.h"
#include "fds.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_delay.h"
#include "nrf_fstorage.h"
#include "nrf_fstorage_sd.h"
#include "ble_dfu.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_bootloader_info.h"
#include "main.h"

#define DEVICE_NAME                     "MajorBLE"                         /**< Name of device. Will be included in the advertising data. */
#define APP_BLE_OBSERVER_PRIO           3                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */
#define APP_BLE_CONN_CFG_TAG            1                                       /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_ADV_INTERVAL                32                                      /**< The advertising interval (in units of 0.625 ms; this value corresponds to 40 ms). */
#define APP_ADV_DURATION                BLE_GAP_ADV_TIMEOUT_GENERAL_UNLIMITED   /**< The advertising time-out (in units of seconds). When set to 0, we will never time out. */
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(100, UNIT_1_25_MS)        /**< Minimum acceptable connection interval (0.5 seconds). */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(200, UNIT_1_25_MS)        /**< Maximum acceptable connection interval (1 second). */
#define SLAVE_LATENCY                   0                                       /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)         /**< Connection supervisory time-out (4 seconds). */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(20000)                  /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (15 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000)                   /**< Time between each call to sd_ble_gap_conn_param_update after the first call (5 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                       /**< Number of attempts before giving up the connection parameter negotiation. */
#define BUTTON_DETECTION_DELAY          APP_TIMER_TICKS(50)                     /**< Delay from a GPIOTE event until a button is reported as pushed (in number of timer ticks). */
#define DEAD_BEEF                       0xDEADBEEF                              /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */


#define LED_CONNECT_BLINK_INTERVAL      APP_TIMER_TICKS(100)  /**< Connect blink interval interval (ticks). */
#define DISCONNECT_INTERVAL       	APP_TIMER_TICKS(10000)  /**< Connect blink interval interval (ticks). */
#define SAMPLES_IN_BUFFER 5

static uint8_t              m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX];  /**< Buffer for storing an encoded advertising set. */

APP_TIMER_DEF(m_disconnect_timer_id);

BLE_WGD_DEF(m_wgd);                                                             /**< LED Button Service instance. */
NRF_BLE_GATT_DEF(m_gatt);                                                       /**< GATT module instance. */
NRF_BLE_QWR_DEF(m_qwr);                                                         /**< Context for the Queued Write module.*/

BLE_ADVERTISING_DEF(m_advertising);                                                 /**< Advertising module instance. */

static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID;                        /**< Handle of the current connection. */
static void advertising_start(void);                                    /**< Forward declaration of advertising start function */
static void advertising_init(void);

static uint8_t wg_type = 0;
static uint64_t wg_key = 0;
static uint64_t wg_debug;
static int8_t tx_power = 0;
static uint8_t ble_adv_timeout = 15;

static volatile uint32_t voltage_falls_detected = 0;
static volatile uint32_t voltage_falls_total    = 0;

void save_tx_power (uint32_t tx_power);
void add_parity(void);
void send_wiegand_data(uint64_t wg_key);

/**@brief Flash event status structure. */
typedef struct
{
    bool							flash_erased;
    bool              flash_written;
    bool  						flash_read;    		
		bool 							disconnect_ena;
		bool 							send_wg_ena;
		bool							ble_adv_ena;
		bool 							update_settings;
		bool 							motion_status;
		bool 							service_mode;
		bool 							setup_beacon;
		bool							re866_dfu_ena;		
} app_status_t;

/**@brief Flash event status structure. */
typedef struct
{
    int8_t							tx_power;
    uint8_t             ble_adv_timeout;
		uint8_t							motion_source;// 0 - internal, 1 - external
} app_settings_t;

app_status_t app_status;
app_settings_t app_settings;

void callback(nrf_fstorage_evt_t * p_evt);
	
NRF_FSTORAGE_DEF(nrf_fstorage_t my_instance) =
{
    .evt_handler    = callback,
    .start_addr     = 0x40000,
    .end_addr       = 0x40FFF,
};

static void advertising_config_get(ble_adv_modes_config_t * p_config)
{
    memset(p_config, 0, sizeof(ble_adv_modes_config_t));

    p_config->ble_adv_fast_enabled  = true;
    p_config->ble_adv_fast_interval = APP_ADV_INTERVAL;
    p_config->ble_adv_fast_timeout  = APP_ADV_DURATION;
}

/**@brief Struct that contains pointers to the encoded advertising data. */
static ble_gap_adv_data_t m_adv_data =
{
    .adv_data =
    {
        .p_data = m_enc_advdata,
        .len    = BLE_GAP_ADV_SET_DATA_SIZE_MAX
    },
    .scan_rsp_data =
    {
        .p_data = NULL,
        .len    = 0

    }
};

static void ble_dfu_evt_handler(ble_dfu_buttonless_evt_type_t event)
{
    switch (event)
    {
        case BLE_DFU_EVT_BOOTLOADER_ENTER_PREPARE:
        {
            //NRF_LOG_INFO("Device is preparing to enter bootloader mode.");

            // Prevent device from advertising on disconnect.
            ble_adv_modes_config_t config;
            advertising_config_get(&config);
            config.ble_adv_on_disconnect_disabled = true;
            ble_advertising_modes_config_set(&m_advertising, &config);

            // Disconnect all other bonded devices that currently are connected.
            // This is required to receive a service changed indication
            // on bootup after a successful (or aborted) Device Firmware Update.            
            //NRF_LOG_INFO("Disconnected %d links.", conn_count);
            break;
        }

        case BLE_DFU_EVT_BOOTLOADER_ENTER:
            // YOUR_JOB: Write app-specific unwritten data to FLASH, control finalization of this
            //           by delaying reset by reporting false in app_shutdown_handler
            //NRF_LOG_INFO("Device will enter bootloader mode.");
            break;

        case BLE_DFU_EVT_BOOTLOADER_ENTER_FAILED:
            NRF_LOG_ERROR("Request to enter bootloader mode failed asynchroneously.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            break;

        case BLE_DFU_EVT_RESPONSE_SEND_ERROR:
            NRF_LOG_ERROR("Request to send a response to client failed.");
            // YOUR_JOB: Take corrective measures to resolve the issue
            //           like calling APP_ERROR_CHECK to reset the device.
            APP_ERROR_CHECK(false);
            break;

        default:
            NRF_LOG_ERROR("Unknown event from ble_dfu_buttonless.");
            break;
    }
}

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

// Connect LED blinking timer handler
static void led_connect_blink_timeout_handler(void * p_context)
{	
	nrf_gpio_pin_toggle(CONN_WG_LED);
}

static void disconnect_timeout_handler(void * p_context)
{	
	app_status.disconnect_ena = true;
}


/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module.
 */
static void timers_init(void)
{
    // Initialize timer module, making it use the scheduler
    ret_code_t err_code = app_timer_init();
    APP_ERROR_CHECK(err_code);
	
    err_code = app_timer_create(&m_disconnect_timer_id, APP_TIMER_MODE_REPEATED, disconnect_timeout_handler);
	
    APP_ERROR_CHECK(err_code);
}

static void writeD0 (void) {

	nrf_gpio_pin_write(D0, 1);
	nrf_delay_us(100);
	nrf_gpio_pin_write(D0, 0);
}

static void writeD1 (void) {
	
	nrf_gpio_pin_write(D1, 1);
	nrf_delay_us(100);
	nrf_gpio_pin_write(D1, 0);
}

void send_wiegand_data (uint64_t number) {
	
	uint64_t mask = 1<<wg_type;
	wg_debug = 0;

  for(uint64_t i = wg_type; i > 0; i--){  
		if( number & ((uint64_t)1<<(uint64_t)(i - 1))/*VB(number,i-2)*/ ){
			writeD1();
			wg_debug <<= 1; 
		}
		else {
			wg_debug |= 1;
			wg_debug <<= 1;
			writeD0();
		}
		mask = mask >> 1;
		nrf_delay_ms(3);
  }
}

void add_parity (void){
	
	uint8_t odd_cnt = 0;
	uint8_t even_cnt = 0;

	// Most significant part
	for(uint64_t i = (wg_type-2); i >= (uint64_t)(wg_type/2); i--){
		if( wg_key & ((uint64_t)1<<(uint64_t)(i-1)) ){
			odd_cnt++;
		}
	}

	// Least significant part
	for(uint64_t i = wg_type/2-1; i > 0; i--){
		if( wg_key & ((uint64_t)1<<(uint64_t)(i-1)) ){
			even_cnt++;
		}
	}

	wg_key = wg_key<<1;

	if (odd_cnt % 2 != 0){				
		wg_key |= (uint64_t)1 << (wg_type-1);
	}
	if (even_cnt % 2 == 0) {
		wg_key |= (uint64_t)1 << 0;
	}
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void)
{
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, NULL);
    APP_ERROR_CHECK(err_code);
}

int8_t read_tx_power(void) {
		
		/* Verify settings is empty before write*/
		static uint32_t number2read = 0;
		ret_code_t rc = nrf_fstorage_read(
				&my_instance,   /* The instance to use. */
				SETTINGS_ADDR,     /* The address in flash where to read data from. */
				&number2read,        /* A buffer to copy the data into. */
				sizeof(number2read)  /* Lenght of the data, in bytes. */
		);
				
		return ((uint8_t)number2read - 256);
}

/**@brief Function for initializing the Advertising functionality.
 *
 * @details Encodes the required advertising data and passes it to the stack.
 *          Also builds a structure to be passed to the stack when starting advertising.
 */
static void advertising_init(void)
{
    ret_code_t    err_code;
		ble_advertising_init_t init;
		ble_adv_modes_config_t adv_modes_config;
	
    ble_uuid_t adv_uuids[] = {{0x6D62, BLE_UUID_TYPE_BLE}};

		static ble_advdata_conn_int_t conn_interval;
		
		conn_interval.min_conn_interval = (uint16_t)MSEC_TO_UNITS(15, UNIT_1_25_MS);
		conn_interval.max_conn_interval = (uint16_t)MSEC_TO_UNITS(125, UNIT_1_25_MS);					
		
		memset(&init, 0, sizeof(init));

    init.advdata.include_appearance      = false;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = adv_uuids;
    init.advdata.p_tx_power_level        = &tx_power;
    init.advdata.p_slave_conn_int        = &conn_interval;
    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = APP_ADV_INTERVAL;
    init.config.ble_adv_fast_timeout  = APP_ADV_DURATION;


    err_code = ble_advertising_init(&m_advertising, &init);
		
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_advertising_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}	
		
		// Configure advertising
		memset(&adv_modes_config, 0, sizeof(adv_modes_config));    		

		advertising_config_get(&adv_modes_config);
		adv_modes_config.ble_adv_on_disconnect_disabled = true;
		ble_advertising_modes_config_set(&m_advertising, &adv_modes_config);
		
		//============================= 
		ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);

}


/**@brief Function for handling Queued Write Module errors.
 *
 * @details A pointer to this function will be passed to each service which may need to inform the
 *          application about an error.
 *
 * @param[in]   nrf_error   Error code containing information about what went wrong.
 */
static void nrf_qwr_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for handling write events to the LED characteristic.
 *
 * @param[in] p_wgd     Instance of LED Button Service to which the write applies.
 * @param[in] led_state Written/desired state of the LED.
 */
static void led_write_handler(uint16_t conn_handle, ble_wgd_t * p_wgd, uint8_t const * value)
{	
	
		/* ================================================== */
		/* Handle Wiegand setting TX power */
		if (value[0] == 0xA0)
		{
			/* @note Supported tx_power values: -40dBm, -20dBm, -16dBm, -12dBm, -8dBm, -4dBm, 0dBm, +3dBm and +4dBm.
			* @note The initiator will have the same transmit power as the scanner.*/			
			
			switch (value[1]) {
				case 1:
						app_settings.tx_power = -40;
					break;
				case 2:
						app_settings.tx_power = -20;
					break;
				case 3:
						app_settings.tx_power = -16;
					break;
				case 4:
						app_settings.tx_power = -12;
					break;
				case 5:
						app_settings.tx_power = -8;
					break;
				case 6:
						app_settings.tx_power = -4;
					break;
				case 7:
						app_settings.tx_power = 0;
					break;					
				case 8:
						app_settings.tx_power = 3;
					break;
				case 9:
						app_settings.tx_power = 4;
					break;
				default:
						app_settings.tx_power = 0;
					break;
			}						
			
			sd_ble_gap_tx_power_set(BLE_GAP_ROLE_PERIPH,m_advertising.adv_handle, app_settings.tx_power);
			app_status.update_settings = true;

			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);			
			
		}
		
		/* ================================================== */
		/* Handle Wiegand gettibg current TX power value */
		/* @note Supported tx_power values: -40dBm, -20dBm, -16dBm, -12dBm, -8dBm, -4dBm, 0dBm, +3dBm and +4dBm.*/
		if (value[0] == 0xA1)
		{
			uint8_t temp_buf[2]= {0x00, 0x00};
			
			switch (app_settings.tx_power) {
				case -40:						 
						temp_buf[1] = 1;
					break;
				case -20:
						temp_buf[1] = 2;
					break;
				case -16:
						temp_buf[1] = 3;
					break;
				case -12:
						temp_buf[1] = 4;
					break;
				case -8:
						temp_buf[1] = 5;
					break;
				case -4:
						temp_buf[1] = 6;
					break;
				case 0:
						temp_buf[1] = 7;
					break;					
				case 3:
						temp_buf[1] = 8;
					break;
				case 4:
						temp_buf[1] = 9;
					break;
				default:
						temp_buf[1] = 0;
					break;
			}	
						
			/* Send response*/
					
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
			
		}
		
		
		/* ================================================== */
		/* Handle Wiegand key */
		if (value[0] == 0x1A || value[0] == 0x22 || value[0] == 0x2A ){
			
			wg_type = value[0];
			wg_key = (uint64_t)value[6]|(uint64_t)value[5]<<8|(uint64_t)value[4]<<16|(uint64_t)value[3]<<24|(uint64_t)value[2]<<32|(uint64_t)value[1]<<40;
	
			// Shift key depending on the protocol
			switch (wg_type){
				case 26:
					wg_key = wg_key>>24;
					break;
				case 34:
					wg_key = wg_key>>16;
					break;
				case 42:
					wg_key = wg_key>>8;
					break;
				default:
					break;
			}
		
			// Send key to the control system
			if (wg_type == 26 || wg_type == 34 || wg_type == 42) {				
                                                                
                                nrf_gpio_pin_set(RELAY);
                                nrf_delay_ms(500);
                                nrf_gpio_pin_clear(RELAY);			
			
                                /* Send response*/
                                uint8_t temp_buf[2]= {0x4F, 0x4B};		
                                ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
								
				
				/* Disconnect from the client */
				nrf_delay_ms(100);				
				sd_ble_gap_disconnect(m_conn_handle,BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);																	
			}
		}
		
		/* ================================================== */
		// Handle reset command
		if (value[0] == 0xB0)
		{
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
			
			nrf_delay_ms(100);
			
			NVIC_SystemReset();
		}
		
		/* ================================================== */
		// Handle request fw id command
		if (value[0] == 0xC0)
		{
			uint8_t fwid_buf[2];
			fwid_buf[0] = (uint8_t)FW_MAJOR;
			fwid_buf[1] = (uint8_t)FW_MINOR;
			
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, fwid_buf);
		}
		
		/* ================================================== */
		// Handle setting advertising timeout
		if (value[0] == 0xD0)
		{
			app_settings.ble_adv_timeout = value[1];
			app_status.update_settings = true;
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		/* ================================================== */
		/* Handle Wiegand gettibg current timeout */
		if (value[0] == 0xD1)
		{	
			uint8_t temp_buf[2]= {0x00, 0x00};
						
			/* Send response*/
			temp_buf[1] = app_settings.ble_adv_timeout;		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);		
		}
		
		/* Handle token */
		if (value[0] == 0xE1 && value[1] == 0xE2 && value[2] == 0xE3)
		{	
			app_timer_stop(m_disconnect_timer_id);
			app_status.service_mode = true;
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		/* Handle relay switching on command		*/
		if (value[0] == 0xF0)
		{	
			nrf_gpio_pin_set(RELAY);
			nrf_delay_ms(100);
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		/* Handle relay switching off command		*/
		if (value[0] == 0xF1)
		{				
			nrf_gpio_pin_clear(RELAY);
			nrf_delay_ms(100);
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		/* Handle relay pulse command for 200 ms */
		if (value[0] == 0xF2)
		{	
			nrf_gpio_pin_set(RELAY);
			nrf_delay_ms(200);
			nrf_gpio_pin_clear(RELAY);			
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		if (value[0] == 0xF3)
		{
			app_status.setup_beacon = true;
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}
		
		if (value[0] == 0xF4)
		{
			app_status.re866_dfu_ena = true;
			
			/* Send response*/
			uint8_t temp_buf[2]= {0x4F, 0x4B};		
			ble_wgd_send_rsp(m_conn_handle, &m_wgd, temp_buf);
		}   	
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    ret_code_t         err_code;
    ble_wgd_init_t     init     = {0};
    nrf_ble_qwr_init_t qwr_init = {0};
		ble_dfu_buttonless_init_t dfus_init = {0};

    // Initialize Queued Write Module.
    qwr_init.error_handler = nrf_qwr_error_handler;

    err_code = nrf_ble_qwr_init(&m_qwr, &qwr_init);
    APP_ERROR_CHECK(err_code);

		dfus_init.evt_handler = ble_dfu_evt_handler;
		
		err_code = ble_dfu_buttonless_init(&dfus_init);
    APP_ERROR_CHECK(err_code);
		
    // Initialize WGD.
    init.led_write_handler = led_write_handler;

    err_code = ble_wgd_init(&m_wgd, &init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module that
 *          are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply
 *       setting the disconnect_on_fail config parameter, but instead we use the event
 *       handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    ret_code_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    ret_code_t             err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{

			
    ret_code_t err_code;

    err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
    ret_code_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
					                                    
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;            
            app_timer_start(m_disconnect_timer_id, DISCONNECT_INTERVAL, NULL);// Enable disconnect from central

            break;

        case BLE_GAP_EVT_DISCONNECTED:
            NRF_LOG_INFO("Disconnected");
            advertising_start();
						            
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            nrf_delay_ms(500);
				
            break;

        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle,
                                                   BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP,
                                                   NULL,
                                                   NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            //NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys =
            {
                .rx_phys = BLE_GAP_PHY_AUTO,
                .tx_phys = BLE_GAP_PHY_AUTO,
            };
            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
            APP_ERROR_CHECK(err_code);
        } break;
				
				case BLE_GAP_EVT_TIMEOUT:					
						
					break;
				

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            //NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            //NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle,
                                             BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            APP_ERROR_CHECK(err_code);
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    ret_code_t err_code;

    err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    // Configure the BLE stack using the default settings.
    // Fetch the start address of the application RAM.
    uint32_t ram_start = 0;
    err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    // Enable BLE stack.
    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    // Register a handler for BLE events.
    NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
    ret_code_t err_code;
    err_code = nrf_pwr_mgmt_init();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
//    if (NRF_LOG_PROCESS() == false)
//    {
        nrf_pwr_mgmt_run();
//    }
}

void in_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    send_wiegand_data(0x4EAE42);
}

void gpio_init(void){

	/* Configure Wiegand interface data lines */
	nrf_gpio_cfg_output(D0);
	nrf_gpio_cfg_output(D1);
	
	/* Configure status LED */
	nrf_gpio_cfg_output(CONN_WG_LED);
	nrf_gpio_pin_clear(CONN_WG_LED);
	
	/* Configure relay control pin */
	nrf_gpio_cfg_output(RELAY);
	nrf_gpio_pin_clear(RELAY);				

}

void wg_init(void)
{
	// Data lines config
	nrf_gpio_pin_clear(D0);
	nrf_gpio_pin_clear(D1);
}

void callback(nrf_fstorage_evt_t * p_evt) {
	
	if (p_evt->id == NRF_FSTORAGE_EVT_ERASE_RESULT) {
		app_status.flash_erased = true;
	}
	
}

void store_settings () {			
	
		/* Verify settings is empty before write*/
		uint32_t number2read = 0;
		ret_code_t rc = nrf_fstorage_read(
				&my_instance,   /* The instance to use. */
				SETTINGS_ADDR,     /* The address in flash where to read data from. */
				&number2read,        /* A buffer to copy the data into. */
				sizeof(number2read)  /* Lenght of the data, in bytes. */
		);
			
		if (number2read != 0xFFFFFFFF) {
				static uint32_t pages_to_erase = 1;
				rc = nrf_fstorage_erase(
						&my_instance,   /* The instance to use. */
						SETTINGS_ADDR,     /* The address of the flash pages to erase. */
						pages_to_erase, /* The number of pages to erase. */
						NULL            /* Optional parameter, backend-dependent. */
				);
				if (rc == NRF_SUCCESS)
				{
						/* The operation was accepted.
							 Upon completion, the NRF_FSTORAGE_ERASE_RESULT event
							 is sent to the callback function registered by the instance. */
						while(app_status.flash_erased == false){							
							nrf_delay_ms(10);
						};
						app_status.flash_erased = false;						
						
				}
				else
				{
						/* Handle error.*/
				}
		}
		
		tx_power = app_settings.tx_power;
		ble_adv_timeout = app_settings.ble_adv_timeout;
				
		uint32_t number2write = (tx_power&0x000000FF) | ((ble_adv_timeout&0x000000FF) << 8);
		
		rc = nrf_fstorage_write(
				&my_instance,   /* The instance to use. */
				SETTINGS_ADDR,     /* The address in flash where to store the data. */
				&number2write,        /* A pointer to the data. */
				sizeof(number2write), /* Lenght of the data, in bytes. */
				NULL            /* Optional parameter, backend-dependent. */
		);
				
		if (rc == NRF_SUCCESS)
		{
				/* The operation was accepted.
					 Upon completion, the NRF_FSTORAGE_READ_RESULT event
					 is sent to the callback function registered by the instance.
					 Once the event is received, it is possible to read the contents of 'number'. */
			nrf_delay_ms(1000);
		}
		else
		{
				/* Handle error.*/
		}
						
//		number2read = 0;
//		
//		rc = nrf_fstorage_read(
//				&my_instance,   /* The instance to use. */
//				SETTINGS_ADDR,     /* The address in flash where to read data from. */
//				&number2read,        /* A buffer to copy the data into. */
//				sizeof(number2read)  /* Lenght of the data, in bytes. */
//		);
//				
//		if (rc == NRF_SUCCESS)
//		{
//				/* The operation was accepted.
//					 Upon completion, the NRF_FSTORAGE_READ_RESULT event
//					 is sent to the callback function registered by the instance.
//					 Once the event is received, it is possible to read the contents of 'number'. */
//			err_code = 0;
//		}
//		else
//		{
//				/* Handle error.*/
//			err_code = 1;
//		}
}

int8_t test_tx_power = 0;


void dispatch_app_status(void){
				
      if (app_status.update_settings) {
              store_settings();
              app_status.update_settings = false;
      }	
      
      if (app_status.disconnect_ena) {
                                      
              app_timer_stop(m_disconnect_timer_id);              
              
              /* We disconnect if client didn't already disconnected by itself */
              if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
              {				
                    ret_code_t err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
                    
                    if (err_code != NRF_ERROR_INVALID_STATE)
                    {
                        APP_ERROR_CHECK(err_code);
                                    
                    }								
              }
              
              app_status.disconnect_ena = false;		
      }
}

void restore_app_settings(){
	
		/* Verify settings is empty before write*/
	
		uint32_t number2read = 0;
	
		ret_code_t rc = nrf_fstorage_read(
				&my_instance,   /* The instance to use. */
				SETTINGS_ADDR,     /* The address in flash where to read data from. */
				&number2read,        /* A buffer to copy the data into. */
				sizeof(number2read)  /* Lenght of the data, in bytes. */
		);
		
		app_settings.tx_power = (int8_t)number2read;
		app_settings.ble_adv_timeout = number2read>>8;
		
		/* Values validation */
		if ((uint8_t)app_settings.tx_power == 0xFF){			
			app_settings.tx_power = DEFAULT_TX_POWER;			
		}
		
		if ((uint8_t)app_settings.ble_adv_timeout == 0xFF || app_settings.ble_adv_timeout == 0x00){			
			app_settings.ble_adv_timeout = DEFAULT_ADV_TIMEOUT;
		}
}


void app_init (void){
	
	nrf_fstorage_init(
			&my_instance,       /* You fstorage instance, previously defined. */
			&nrf_fstorage_sd,   /* Name of the backend. */
			NULL                /* Optional parameter, backend-dependant. */
	);
	
	app_settings.motion_source = 0;
	app_status.setup_beacon = false;
	restore_app_settings();
	sd_ble_gap_tx_power_set(BLE_GAP_ROLE_PERIPH, m_advertising.adv_handle, app_settings.tx_power);			
}

/**@brief Function for initializing the nrf log module.
 */
static void log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
    APP_ERROR_CHECK(err_code);

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}


int main(void)
{ 							
		#if DFU_ENA
			// Initialize the async SVCI interface to bootloader before any interrupts are enabled.
	
			uint32_t err_code = ble_dfu_buttonless_async_svci_init();
			APP_ERROR_CHECK(err_code);
		#endif		
    log_init();    
    NRF_LOG_ERROR(" Program started ");

    gpio_init();
    wg_init();
    timers_init();						
    power_management_init();						
    ble_stack_init();
    gap_params_init();
    gatt_init();
    services_init();
    advertising_init();
    conn_params_init();

    app_init();
    advertising_start();
				
    // Enter main loop.
    for (;;)
    {
      dispatch_app_status();				
      idle_state_handle();
    }
}

/**
 * @}
 */
