#include "nrf_gpio.h"
#include "app_timer.h"

#include "depassivation.h"

// ==========================================================================================

#define DEPAS_SWITCH                    NRF_GPIO_PIN_MAP(0, 26)    // P0.26, GPIO[14]

#define PASSIVATION_TIME_MS             24*60*60*1000
#define DEPASSIVATION_TIME_MS           1000

// ==========================================================================================

APP_TIMER_DEF(depassivation_timer_id);
static bool is_depassivation_active = false;

// ==========================================================================================

static void depassivation_start() {
    nrf_gpio_pin_set(DEPAS_SWITCH);
}

static void depassivation_stop() {
    nrf_gpio_pin_clear(DEPAS_SWITCH);
}

static void depassivation_timer_start(uint32_t ticks) {
    ret_code_t err_code = app_timer_start(depassivation_timer_id, ticks, NULL);
    APP_ERROR_CHECK(err_code);
}

static void depassivation_timer_handler(void * p_context) {
    if (is_depassivation_active) {
        depassivation_stop();
        depassivation_timer_start(PASSIVATION_TIME_MS);
    }
    else {
        depassivation_start();
        depassivation_timer_start(DEPASSIVATION_TIME_MS);
    }
}

static void depassivation_timer_create() {
    ret_code_t err_code = app_timer_create(&depassivation_timer_id, APP_TIMER_MODE_SINGLE_SHOT, depassivation_timer_handler);
    APP_ERROR_CHECK(err_code);
}

// ==========================================================================================

void depassivation_init() {
    nrf_gpio_cfg_output(DEPAS_SWITCH);
    depassivation_stop();
    depassivation_timer_create();
    depassivation_timer_start(PASSIVATION_TIME_MS);
}