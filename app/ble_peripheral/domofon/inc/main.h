/*

Device-specific definitions

*/

#define HW_VER 11

/* Pin assignment macros section */

#define D0            NRF_GPIO_PIN_MAP(0, 14)						
#define D1            NRF_GPIO_PIN_MAP(0, 15)						
#define RELAY         NRF_GPIO_PIN_MAP(0, 5)						
#define MOTION        NRF_GPIO_PIN_MAP(0, 4)							
#define CONN_WG_LED   NRF_GPIO_PIN_MAP(0, 17)						


#if HW_VER == 10
	#define RX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0,29)
#else												
	#define RX_PIN_NUMBER  NRF_GPIO_PIN_MAP(0,13)
#endif

#define TX_PIN_NUMBER  NRF_GPIO_PIN_MAP(1,13)
#define CTS_PIN_NUMBER NRF_GPIO_PIN_MAP(1,12)
#define RTS_PIN_NUMBER NRF_GPIO_PIN_MAP(0,31)

/* End of pin assignment macros*/

#define SETTINGS_ADDR									0x40000
#define FW_MAJOR											1
#define FW_MINOR											13
#define DFU_ENA												0
#define SENSOR_EVT_MAX_CNT						5
#define DEFAULT_TX_POWER							0
#define DEFAULT_ADV_TIMEOUT						15
#define MOTION_DETECTION_PERIOD				5
#define DOPPLER_ON_START_DELAY				25

#define UART_TX_BUF_SIZE              1024                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE              1024                                         /**< UART RX buffer size. */

#define B(bit_no)         (1 << (bit_no))
#define CB(reg, bit_no)   (reg) &= ~B(bit_no)
#define SB(reg, bit_no)   (reg) |= B(bit_no)
#define VB(reg, bit_no)   ( (reg) & B(bit_no) )
#define TB(reg, bit_no)   (reg) ^= B(bit_no)
